# Program sends Rpi3'CPU temperature to the cloud

try:
    import os
    import sys
    import datetime
    import time
    import boto3
    import threading
    print("All Modules Loaded ...... ")
except Exception as e:
    print("Error {}".format(e))


class MyDb(object):

    def __init__(self, Table_Name='TempDB'):
        self.Table_Name = Table_Name
        self.db         = boto3.resource('dynamodb')
        self.table      = self.db.Table(Table_Name)
        self.client     = boto3.client('dynamodb')

    @property
    def get(self):
        response = self.table.get_item(
            Key={
                'Time':"1"
            }
        )
        return response

    def put(self, Time='', Temperature=''):
        self.table.put_item(
            Item={
                'Time':Time,
                'Temperature':Temperature
            }
        )

    def delete(self, Time=''):
        self.table.delete_item(
            Key={
                'Time':Time
            }
        )

    def describe_table(self):
        response = self.client.describe_table(
            TableName='TempDB'
        )
        return response

    @staticmethod
    def sensor_value():
        # get the CPU temperature by executing shell command
        stream      = os.popen('vcgencmd measure_temp')
        temperature = stream.read().strip('temp=')[:-3]
        return temperature


def main():
    global counter

    threading.Timer(interval=10, function=main).start()
    obj = MyDb()
    Temperature = obj.sensor_value()
    obj.put(Time=str(counter), Temperature=str(Temperature))
    counter = counter + 1
    print("Uploaded Sample on Cloud T:{} ".format(Temperature))

if __name__ == "__main__":
    global counter
    counter = 0
    main()
