import boto3

# Get the service resource.
dynamodb = boto3.resource('dynamodb')

# Create the DynamoDB table.
table = dynamodb.create_table(
    TableName='TempDB',
    KeySchema=[
        {
            'AttributeName': 'Time',
            'KeyType': 'HASH'
        },
        {
            'AttributeName': 'Temperature',
            'KeyType': 'RANGE'
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'Time',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'Temperature',
            'AttributeType': 'S'
        },
    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 5,
        'WriteCapacityUnits': 5
    }
)

# Wait until the table exists.
table.meta.client.get_waiter('table_exists').wait(TableName='TempDB')
table.put_item(
   Item={
        'Time': '0.0',
        'Temperature': '25.0',
    }
)
# Print out some data about the table.
print(table.item_count)
